print("IT/ICT Student Percentage")

print("Input:")
numIT = float(input("Enter number of IT students: "))
numICT =float(input("Enter number of ICT students: "))

numTotal = numICT+numIT
pctIT = (numIT/numTotal)*100
pctICT = (numICT/numTotal)*100

print("Output:")
print("Total Students:\t{0:.2f}".format(numTotal))
print("IT Students:\t{0:.2f}%".format(pctIT))
print("ICT Students:\t{0:.2f}%".format(pctICT))
