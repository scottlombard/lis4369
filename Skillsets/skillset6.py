print('Python Lists\n')
print('Input:')

length = int(input('Enter number of list elements: '))
my_list = []

for x in range(length): 
    #Fix input below to output index number
    item = str(input('Please enter list element ' + str(x+1) + ': '))
    my_list.append(item.rstrip('\r'))

print('Output:')
print(my_list)

new_item = input('Please enter list element: ')
ins_index = int(input('Please enter list *index* position: '))
ins_index-=1

print('Insert element into specific position in my_list:')
my_list.insert(ins_index, new_item)
print(my_list)

print('\nCount number of elements in list:')
print(len(my_list))

print('\nSort elements in list alphabetically:')
print(sorted(my_list))

print('\nReverse list:')
print(sorted(my_list, reverse=True))

print('\nRemove last list element:')
my_list.pop()
print(my_list)

print('\nDelete second element from list by *index*')
my_list.pop(1)
print(my_list)

print('\nDelete elemt from list by *value* (cherries):')
print(my_list)

print('\nDelete all elements from list:')
del my_list[:]
print(my_list)

print("bye")