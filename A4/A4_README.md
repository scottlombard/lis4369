# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Assignment 4 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### Assignment Screenshots:
*Screenshot of data_analysis_2(Visual Studio Code)*:
![See images folder](images/A4_part1.PNG)
![See images folder](images/A4_part2.PNG)
![See images folder](images/A4_part3.PNG)
![See images folder](images/A4_part4.PNG)
![See images folder](images/A4_part5.PNG)
![See images folder](images/A4_part6.PNG)
![See images folder](images/A4_part7.PNG)
![See images folder](images/A4_part8.PNG)
![See images folder](images/A4_part9.PNG)
![See images folder](images/A4_part10.PNG)

![See images folder](images/A4_graph.PNG)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/