# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/A1/A1_README.md/)

    * Install Python

    * Install R

    * Install Visual Studio Code

    * Create a1_tip_calculator application

    * provide scrresnots of installations

    * Create Bitbucket Repo

    * Complete Bitbucket tutorial (bitbucketstationlocations)

    * Provide git command descriptions

2.  [A2 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/A2/A2_README.md)

    * Payroll Calculator

3. [A3 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/A3/A3_README.md)

    * Paint Estimator

4. [A4 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/A4/A4_README.md)


    * Data Analysis 2

5. [A5 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/A5/A5_README.md)

    * R

6. [P1 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/P1/P1_README.md)

    * Project 1 (demo.py)

7. [P2 README.md](https://bitbucket.org/scottlombard/lis4369/src/master/P2/P2_README.md)

    * Project 2 (R)
