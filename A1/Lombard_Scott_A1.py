print('Tip Calculator\n')
print('Program Requirements:')
print('1. Must use float data typr for user input (except, "Party Number").')
print('2. Must round calculatuons to two decimal places.')
print('3. Must forma currency with dollar sign, and two decimal places.\n')
print('User Input:\n')

mealCost = input('Cost of meal: ')
mealCost = float(mealCost)

taxPerc = input('Tax percent: ')
taxPerc = float(taxPerc)

tipPerc = input('Tip percent: ')
tipPerc = float(tipPerc)

partyNum = input('Party number: ')
partyNum = int(partyNum)

print('\nProgram Output:')
print("Subtotal: ${0:.2f}".format(mealCost))

taxFctr = (taxPerc/100)
tax = (taxFctr*mealCost)
print("Tax: ${0:.2f}".format(tax))

amtDue = (mealCost + tax)
print("Amount Due: ${0:.2f}".format(amtDue))

tipFctr = (tipPerc/100)
tip = (tipFctr*amtDue)
print("Gratuity: ${0:.2f}".format(tip))

total = (amtDue + tip)
print("Total: ${0:.2f}".format(total))

split = (total/partyNum)
print("Split ("+str(partyNum) +"): ${0:.2f}".format(split))




