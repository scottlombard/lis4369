print('Payroll Calculator\n')
def get_requirements():
    print('Program Requirements:')
    print('1. Must use float data type for user input").')
    print('2. Overtime rate: 1.5 times hourly rate (hours over 40).')
    print('3. Holiday rate: 2.0 times hourly rate (all holiday hours).')
    print('4. Must format currency with dollar sign, and round to two decimal places')
    print('5. Create ar least three function that are all called by the program:')
    print('\ta. main(): callas at least two other functions.')
    print('\tb. get_requirements(): displays the program requirements')
    print('\tc. calculate_payroll(): calculates an indivudal one-week paycheck\n')

def calculate_payroll():
    overtime_rate = 1.5
    holiday_rate = 2.0
   
    print('Input:')
    hours_worked = float(input('Enter hours worked: '))
    holiday_hours = float(input('Enter holiday hours: '))
    pay_rate = float(input('Enter hourly pay rate: '))
    
    if hours_worked > 40:
        base = float(40*pay_rate)
        overtime = float((hours_worked-40)*overtime_rate*pay_rate)

    else:
        base = float(hours_worked*pay_rate)
        overtime = float(0)

    holiday = float(holiday_hours*holiday_rate*pay_rate)
    gross = float(base+overtime+holiday)

    print('\nOutput: ')
    print('Base:\t\t${0:.2f}'.format(base))
    print('Overtime:\t${0:.2f}'.format(overtime))
    print('Holiday:\t${0:.2f}'.format(holiday))
    print('Gross:\t\t${0:.2f}'.format(gross))

def main():
    get_requirements()
    calculate_payroll()

main()


