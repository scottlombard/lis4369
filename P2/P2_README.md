# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Project 2 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### Assignment Screenshots:
*Screenshots of LIS4369_P2.R(Visual Studio Code)*:
![See images folder](images/part1.PNG)
![See images folder](images/part2.PNG)
![See images folder](images/part3.PNG)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/