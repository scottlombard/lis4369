import pandas as pd
import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print("Data Analysis 1\n")
    print("Program Requirements:")
    print("1. Run demo.py.")
    print("2. If errors, more than likely installations.")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations:")
    print("\ta. pandas (only if missing)")
    print("\tb. pandas-datareader (only if missing)")
    print("\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays the program requirements.")
    print("\tc. data_analysis_1(): displays the following data.")

def data_analysis_1():
    start = datetime.datetime(2010,1,1)
    end = datetime.datetime(2018,10,15)

    df = pdr.DataReader("XOM","yahoo",start,end)

    print("\nPrint number of records: ")
    print(df.shape[0])

    print("\nPrint columns: ")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first 5 lines: ")
    print(df.head())

    print("\nPrint last 5 lines: ")
    print(df.tail())

    print("\nPrint first 2 lines: ")
    print(df.head(2))

    print("\nPrint last 2 lines: ")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()

    print("Bye")

def main():
    get_requirements()
    data_analysis_1()

if __name__ == "__main__":
    main()