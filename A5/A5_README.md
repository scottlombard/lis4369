# LIS4369 - Extensible Enterprise Solutions

## Scott Lombard

### Assignment 5 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Questions
3. Bitbucket repo links
	a) This assignment

#### R Tutorial Screenshots:

*Screenshots of the R tutorial*:

![See images folder1](images/realtutorial.png)
![See images folder2](images/p1.jpeg)
![See images folder3](images/p2.jpeg)
![See images folder4](images/p3.jpeg)
![See images folder5](images/p4.jpeg)
![See images folder6](images/p5.jpeg)

#### Assignment 5 Screenshots:

*Screenshots Assignment 5*:

![See images folder](images/A5_Rstudio.png)
![See images folder](images/A5_Rstudio2.png)

##### Repo:
*https://bitbucket.org/scottlombard/bitbucketstationlocations/src/master/